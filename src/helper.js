export function RGB(r, g, b)
{
	return (r << 16) + (g << 8) + b;
}
