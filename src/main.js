import Game from "./game/game.js";
import ScriptRunner from "./script/script-runner.js";
import conf from "./config.js";

import * as intro from "./story/intro.js";
import dayCycle from "./story/day-cycle.js";
import logic from "./story/logic/logic.js";

function playGame()
{
    const scriptRunner = new ScriptRunner(new Game(conf));
    scriptRunner.play(intro.intro)
        .then(() => scriptRunner.play(dayCycle));
}

function testGame()
{
    conf.developer = true;

    const scriptRunner = new ScriptRunner(new Game(conf));

	// logic.me.money.set(10000);
	// logic.g.affection.set(3);

    scriptRunner.play(dayCycle);
}

function main() {
    playGame();
    // testGame();
}

window.onload = main;