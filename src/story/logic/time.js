const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
const dayTimes = ['Morning', 'Day', 'Evening'];

function nextInCycle(elements, current, doCycle)
{
    for (let i = 0; i < elements.length; i++)
    {
        if (current === elements[i])
        {
            if (i < elements.length - 1)
            {
                return elements[i + 1];
            }
            else
            {
                if (doCycle)
                {
                    return elements[0];
                }
                else
                {
                    return current;
                }
            }
        }
    }
    return elements[0];
}

export default class Time
{
    constructor()
    {
        this.day = days[0];
        this.dayTime = dayTimes[0];
        this.dayTimeActionSpent = false;
    }

    get isWeekend()
    {
        return this.day === days[5] || this.day === days[6];
    }

    get isStudyDay()
    {
        return !this.isWeekend;
    }

    nextDay()
    {
        this.day = nextInCycle(days, this.day, true);
        this.dayTime = dayTimes[0];
        this.dayTimeActionSpent = false;
    }

    isMorning()
    {
        return this.dayTime === dayTimes[0];
    }

    isMiddleDay()
    {
        return this.dayTime === dayTimes[1];
    }

    isEvening()
    {
        return this.dayTime === dayTimes[2];
    }

    nextDayTime()
    {
        this.dayTime = nextInCycle(dayTimes, this.dayTime, false);
        this.dayTimeActionSpent = false;
    }

    spendDayTime()
    {
        this.dayTimeActionSpent = true;
    }

    get isDayTimeActionSpent()
    {
        return this.dayTimeActionSpent;
    }
}
