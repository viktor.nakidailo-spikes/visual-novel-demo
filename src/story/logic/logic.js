import Time from "./time.js";

class MinZeroNumber
{
    constructor(value)
    {
        this.value = value;
    }

    plus(amount)
    {
        this.set(this.value + amount);
    }

    minus(amount)
    {
        this.set(this.value - amount);
    }

    get()
    {
        return this.value;
    }

    set(value)
    {
        this.value = Math.max(0, value);
    }
}

class Logic
{
    constructor()
    {
        this.time = new Time();

        this.me = {
            fatigue: new MinZeroNumber(0),
            fatigueTreshold: 100,
            money: new MinZeroNumber(200),
            dateCost: 100,

            get isFatigueLow()
            {
                return this.fatigue.get() < this.fatigueTreshold;
            },

            get isCashEnoughForDate()
            {
                return this.money.get() >= this.dateCost;
            },
        };

        this.g = {
            affection: new MinZeroNumber(0),
        };

        this.smalltalk = {
            oldManAndSeaStage: 0,
        };

    }

    get isDayTimeActionSpent()
    {
        return this.time.isDayTimeActionSpent;
    }

    get isCashEnoughForDate()
    {
        return this.me.isCashEnoughForDate;
    }

    nextDay()
    {
        this.time.nextDay();
    }

    nextDayTime()
    {
        this.time.nextDayTime();
    }

    isMorning()
    {
        return this.time.isMorning();
    }

    isMiddleDay()
    {
        return this.time.isMiddleDay();
    }

    isEvening()
    {
        return this.time.isEvening();
    }

    workPartTime()
    {
        this.me.money.plus(100);
        this.me.fatigue.plus(33);

        this.time.spendDayTime();
    }

    hangoutAlone()
    {
        this.me.fatigue.minus(100);
        
        this.time.spendDayTime();
    }

    dateFinished(affectionGain)
    {
        this.g.affection.plus(affectionGain);

        this.me.money.minus(100);
        this.me.fatigue.minus(33);

        this.time.spendDayTime();
    }

}

var logic = new Logic();
export default logic;
