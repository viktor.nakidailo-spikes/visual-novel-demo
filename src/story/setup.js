import conf from "../config.js";
import createTag from "../script/create-tag.js";
import Say from "../script/elements/say.js";
import * as Images from "../script/elements/images.js";
import Scene from "../script/elements/scene.js";

export const helper = {
    fullscreen: new PIXI.Rectangle(0, 0, conf.app.width, conf.app.height),
};

export const me = createTag((str) => new Say(conf.me.name, conf.me.nameStyle, str));
export const g = createTag((str) => new Say(conf.g.name, conf.g.nameStyle, str));
g.toString = () => conf.g.name;
g.show = () => new Images.Show(conf.g.image, conf.g.rect);
g.hide = () => new Images.Hide(conf.g.image);
let textStyleForColor = (color) => Object.assign({}, conf.textPresenter.style, {fill: color});
export const colored = (color, text) => new Say('', 0, text, textStyleForColor(color));
export const scene = (name) => new Scene(`./img/bg/${name}.jpg`, helper.fullscreen);
