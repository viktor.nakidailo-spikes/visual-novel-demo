import {me, g, helper, scene} from "./setup.js";

import * as Images from "../script/elements/images.js";
import Scene from "../script/elements/scene.js";

export const intro = [
    scene('darkness'),

    `...`,
    "......",
    ".........",

    scene('my-room'),

    me`Huh.`,
    "I woke up.",
    "...",
    "Slowly recovering, I've remembered what happened yesterday.",
    me`I've met with ${g}.`,
    "We've had some meaningless but fun talk, chatted about different things.",
];

