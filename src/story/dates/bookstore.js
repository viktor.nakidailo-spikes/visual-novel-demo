import {me, g, helper, scene} from "../setup.js";
import * as random from "../../util/random.js";
import logic from "../logic/logic.js";

function* smalltalk()
{
    let rnd = random.integer(0, 3);
    if (0 == rnd)
    {
        yield oldManAndSea;
    }
    else if (1 == rnd)
    {
        yield comicsOne;
    }
    else if (2 == rnd)
    {
        yield comicsTwo;
    }
}

function oldManAndSea()
{

    if (!logic.smalltalk.oldManAndSeaStage)
    {
        logic.smalltalk.oldManAndSeaStage = 1;
        return [
            g.show(),
            g `Hey, have you read this book?`,
            `${g} is holding \`The Old Man and the Sea\` by Ernest Hemingway. Cover depicts a sea, a fish and an old man.`,
            me `No.`,
            g `...`,
        ];
    }
    else if (1 === logic.smalltalk.oldManAndSeaStage)
    {
        logic.smalltalk.oldManAndSeaStage = 2;
        return [
            me `I've read that Hemingway book.`,
            g.show(),
            g `...\`The Old Man and the Sea\`?`,
            me `Yeah.`,
            g `Did you like it?`,
            me `You know, maybe that old man should've realized the fish was too big for him and would just leave it alone.`,
            g `...`,
            g `Well, maybe this fish is too big for you!`,
            me `...What?`,
        ];
    }
    else
    {
        return [
            me `About that Hemingway book.`,
            g.show(),
            g `...`,
            g `Just...`,
            g `Forget it.`,
            me `...`,
        ];
    }
}

function comicsOne()
{
    return [
        me `Do you read any comics?`,
        g.show(),
        `${g} turns to me and ponders for a minute.`,
        g `Hmm...`,
        g `There was this comic about a guy who must defeat a girl's seven evil exes to date her.`,
        me `...`,
        g `I liked it.`,
        g `He-he.`,
    ];
}

function comicsTwo()
{
    return [
        g.show(),
        g `That reminds me...`,
        g `I've read one webcomic recently, about a woman and her robot.`,
        g `...Strange one.`,
        g `...`,
        me `How come?`,
        g `Well, they did... stuff.`,
        me `...Stuff?`,
        g `I'm not telling you.`,
    ];
}

const Place = 
{
    intro()
    {
        return [
            `We visit bookstore with ${g}.`,
            scene('bookstore'),
        ];
    },

    smalltalk()
    {
        return smalltalk;
    },
}

export default Place;
