import {me, g, scene} from "../setup.js";
import * as random from "../../util/random.js";

function shh()
{
    return [
        g `Shhh!...`,
        me `...`,
        ];
}

function wow()
{    
    return [
        g `Wow...`,
        me `...`,
        g `*chuckle*`,
        ];
}

function cmon()
{
    return [
        me `C'mon! How can they do this? Oh, man...`,
        g `*chuckle*`,
        g `Shh... Don't cry! *chuckle*`,
        ];
}

function* smalltalk()
{
    let rnd = random.integer(0, 3);

    if (0 === rnd)
    {
        yield shh;
    }
    else if (1 === rnd)
    {
        yield wow;
    }
    else if (2 === rnd)
    {
        yield cmon;
    }
}

const Place = 
{
    intro()
    {
        return [
            `We went to cinema with ${g}.`,
            scene('cinema'),
        ];
    },

    smalltalk()
    {
        return smalltalk;
    },
}

export default Place;
