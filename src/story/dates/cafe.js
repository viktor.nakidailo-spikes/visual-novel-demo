import {me, g, helper, scene} from "../setup.js";
import * as random from "../../util/random.js";

function* smalltalk()
{
    let rnd = random.integer(0, 3);
    if (0 == rnd)
    {
        yield myCake;
    }
    else if (1 == rnd)
    {
        yield terribleMistake;
    }
    else if (2 == rnd)
    {
        yield notFat;
    }
}

function myCake()
{

	return [
		g.show(),
	    g `You're eating your cake?`,
	    me `Yeah.`,
	    g `...`,
	    g `Are you sure? Because right now you're not eating your cake.`,
	    me `...Do you want my cake?`,
	    g `...Yes.`,
	    me `Well, you can get half.`,
	    g `Thank you!`,
	];
}

function terribleMistake()
{
	return [
		g.show(),
	    g `Aaah!.. I made a terrible mistake!`,
	    me `What? What?`,
	    g `I should've ordered a strawberry cake instead!`,
	    me `*chuckle*`,
	    g `Hey! Don't laugh!`,
	];
}

function notFat()
{
	return [
		g.show(),
	    `${g} stares at me and asks,`,
	    g `...Do you think I'm fat?`,
	    me `...No.`,
	    `${g} turns around.`,
		g.hide(),
	    g `Excuse me, can I have this chocolate parfait?`,
	];
}

const Place = 
{
    intro()
    {
        return [
			`We sit at cafe with ${g}.`,
			scene('cafe'),
        ];
    },

    smalltalk()
    {
        return smalltalk;
    },
}

export default Place;
