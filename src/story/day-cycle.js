import conf from '../config.js';
import {me, g, helper, colored, scene} from "./setup.js";
import logic from "./logic/logic.js";

import * as Images from "../script/elements/images.js";
import Scene from "../script/elements/scene.js";
import Menu from "../script/elements/menu.js";

import Cinema from "./dates/cinema.js";
import Cafe from "./dates/cafe.js";
import Bookstore from "./dates/bookstore.js";
import * as random from "../util/random.js";

function date(place)
{
    return function*() {
        let hasSmalltalk = random.integer(0, 3) === 0;

        let affectionGain = 1;
        if (hasSmalltalk)
            affectionGain = 2;
        
        yield [
            place.intro(),
            "...",
            "......",
            ];

        if (hasSmalltalk)
        {
            yield [
            place.smalltalk(),
            g.hide(),
            ];
        }

        yield [
            "...",
            "......",
            scene('darkness'),
            () => logic.dateFinished(affectionGain),
            ];

        if (hasSmalltalk)
        {
            yield colored(conf.colors.pink, `${g}'s affection increased strongly!`);
        }
        else
        {
            yield colored(conf.colors.pink, `${g}'s affection increased!`);
        }
    };
}


function dateMenu(){
    return new Menu(`Choose a date.`,[
        ["cinema",
            date(Cinema)],
        ["cafe",
            date(Cafe)],
        ["bookstore",
            date(Bookstore)],
        ["back", 
            []],
    ]);
};

function* dayTimeChoices()
{
    let choices = [
        ["part time work",
            function*() {
                if (!logic.me.isFatigueLow)
                    yield [
                        "I'm too tired.",
                        ];
                else
                    yield  [
                        "I go work part-time for some time.",
                        "...",
                        "......",
                        ".........",
                        () => logic.workPartTime(),
                        colored(conf.colors.gold, "Cash increased!"),
                        ];
            },
        ],
        [`date with ${g} at...`,
            function* () {
                if (!logic.isCashEnoughForDate)
                    yield "I don't have enough cash...";
                else
                    yield dateMenu();
            }
        ],
        ["hang out alone",
            [
                "I hang out alone for some time.",
                "...",
                () => logic.hangoutAlone(),
            ]
        ],
    ];

    if (conf.developer)
    {
        choices.push([
            'developer',
            new Menu(
                'developer',
                [
                    [
                        `date`,
                        dateMenu()
                    ],
                    [      
                        `back`,
                        []
                    ]
                ]
            )
        ]);
    }

    do 
    {
        let menuString = `${logic.time.day}, ${logic.time.dayTime}.`;
        if (!logic.me.isFatigueLow)
            menuString += ` I'm TIRED.`;
        menuString += ` Got ${logic.me.money.get()}$.`;

        yield new Menu(
            menuString,
            choices
        );
        yield g.hide();

    } while (!logic.isDayTimeActionSpent)
}

export default function* dayCycle()
{
    while (true)
    {
        yield scene('darkness');
        yield `${logic.time.day}.`;

        if (logic.time.isStudyDay)
        {
            yield [
                `I go to study.`,
                "......",
                ".........",
            ];
        }
        else
        {
            if (logic.time.isWeekend)
            {
                yield `It's a weekend! Yay!`;
            }

            yield dayTimeChoices();
        }

        logic.nextDayTime();

        yield dayTimeChoices();

        logic.nextDayTime();

        yield dayTimeChoices();

        yield "Day end.";
        yield "...";

        logic.nextDay();
    }
}
