export default class Say
{
    constructor(name, nameStyle, text, textStyle)
    {
        this.name = name;
        this.nameStyle = nameStyle;
        this.text = text;
        this.textStyle = textStyle;
    }

    async do(game)
    {
        game.setName(this.name, this.nameStyle);

        let runner = () => new Promise(
            (resolve) => game.typingText(this.text, this.textStyle, resolve)
            );
        await runner();
    }
}
