let sceneBg = null;

export default class Scene
{
    constructor(imagePath, rect)
    {
        this.imagePath = imagePath;
        this.rect = rect;
    }

    do(game)
    {
        if (sceneBg)
        {
            game.stage.removeChild(sceneBg);
        }
        
        sceneBg = new PIXI.Sprite.fromImage(this.imagePath);
        game.stage.addChildAt(sceneBg, 0);

        if (this.rect)
        {
            sceneBg.x = this.rect.x;
            sceneBg.y = this.rect.y;
            sceneBg.width = this.rect.width;
            sceneBg.height = this.rect.height;
        }
    }
}
