export default class Menu
{
	constructor(text, choices)
	{
		this.text = text;
		this.choices = choices;
	}

	async do(game)
	{
		let choices = this.choices.map((choice) => choice[0]);
        let runner = () => new Promise((resolve) => game.menu(this.text, choices, resolve));
        let clickedIdx = await runner();

		return this.choices[clickedIdx][1];
	}
}