class ImageShower
{
    constructor()
    {
        this.shown = {};
        this.shownInOrder = [];
    }

    _hide(game, imagePath)
    {
        if (!imagePath)
        {
            return;
        }

        if (this.shown[imagePath])
        {
            game.stage.removeChild(this.shown[imagePath]);

            let index = this.shownInOrder.indexOf(imagePath);
            if (index > -1) 
            {
                this.shownInOrder.splice(index, 1);
            }
            
            delete this.shown[imagePath];
        }
    }

    show(game, imagePath, rect)
    {
        this._hide(game, imagePath);

        let image = game.stage.addChild(PIXI.Sprite.fromImage(imagePath));
        if (rect)
        {
            image.x = rect.x;
            image.y = rect.y;
            image.width = rect.width;
            image.height = rect.height;
        }

        this.shown[imagePath] = image;
        this.shownInOrder.push(imagePath);
    }

    hideLast(game)
    {
        this._hide(game, this.shownInOrder[this.shownInOrder.length - 1]);
    }

    hide(game, imagePath)
    {
        this._hide(game, imagePath);
    }
}

let _imageShower = new ImageShower();

export class Show
{
    constructor(imagePath, rect)
    {
        this.imagePath = imagePath;
        this.rect = rect;
    }

    do(game)
    {
        _imageShower.show(game, this.imagePath, this.rect);
    }
}

export class HideLast
{
    do(game)
    {
        _imageShower.hideLast(game);
    }
}


export class Hide
{
    constructor(imagePath)
    {
        this.imagePath = imagePath;
    }

    do(game)
    {
        _imageShower.hide(game, this.imagePath);
    }
}