export default function createTag(func)
{
    return (strings, ...expressions) => {
        let str = '';
        for (let i = 0; i < strings.length; i++)
        {
            let string = strings[i];
            let expr = '';
            if (expressions !== undefined)
            {
                if (expressions[i] !== undefined)
                {
                    expr = expressions[i];
                }
            }
            str += string + expr;
        }
        return func(str);
    }
}
