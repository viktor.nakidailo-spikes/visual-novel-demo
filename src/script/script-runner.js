import Say from "./elements/say.js";

export default class ScriptRunner
{
    constructor(game)
    {
        this.game = game;
    }

    async play(something)
    {
        if (something === undefined)
        {
            return;
        }

        if (typeof something === 'string')
        {
            something = new Say("", 0x0, something);
        }
        else
        {
            if (typeof something[Symbol.iterator] === 'function' )
            {
                await this._iterate(something);
                return;
            }

            if (typeof something === 'function')
            {
                await this.play(something());
                return;
            }
        }

        let res = await something.do(this.game);

        await this.play(res);
    }

    async _iterate(iteratable)
    {
        for (let el of iteratable)
        {
            await this.play(el);
        }
    }
}
