import * as helper from './helper.js';

var config = {
    app: {
        width: 2560,
        height: 1440
    },
    textBg: {
        image: './img/textbox.png',
        rect: new PIXI.Rectangle(150, 1050, 2260, 380)
    },
    clickIndicator: {
        image: './img/bunny.png',
        position: new PIXI.Point(2360, 1330)
    },
    textPresenter: {
        style: {
            fontFamily: "Arial", 
            fontSize: "60px", 
            fill: helper.RGB(255, 255, 255),
            strokeThickness: 2,
        },
        rect: new PIXI.Rectangle(450, 1200, 2560 - 800, 200),
        charsPerSec: 11,
    },

    menu: {
        elementHeight: 200,
        center: new PIXI.Point(2560 / 2, 1200 / 2),
        buttons: {
            image: './img/textbox.png',
            rect: new PIXI.Rectangle(0, 0, 500, 100),
            textStyle: {
                fontFamily: "Arial", 
                fontSize: "90px", 
                align: "center",
                fill: 0x4993af,
            },
        },
    },

    colors: {
        gold: helper.RGB(255, 228, 117),
        pink: helper.RGB(225, 127, 112),
    },
};

config.nameDisplayer = {
    style: {
        fontFamily: "Arial", 
        fontSize: "90px", 
        align: "left",
    },
    position: new PIXI.Point(310, 1110),
};

config.me = {
    name: 'Me',
    nameStyle: Object.assign(
        {},
        config.nameDisplayer.style,
        {
            fill: 0x4993af,
            dropShadow: true,
        },
    ),
};

config.g = {
    name: 'Penelope',
    nameStyle: Object.assign(
        {},
        config.nameDisplayer.style,
        {
            fill: config.colors.pink,
            dropShadow: true,
            dropShadowAlpha: 0.5,
        },
    ),
    image: './img/chars/g.png',
    rect: new PIXI.Rectangle(1280, 1440 - 1350, 654.5, 1397),
};

export default config;