export default class TypingText extends PIXI.Text 
{
    constructor(text, style, rect, charsPerSec, canvas)
    {
        style.wordWrap = true;
        style.wordWrapWidth = rect.width;
        
        super(text, style, canvas);

        this.setTransform(rect.x, rect.y);

        this.text = text;
        this.style = style;
        this.rect = rect;
        this.charsPerSec = charsPerSec;
        this.elapsed = 0;

        this.tickerFunction = (targetFramesPassed) => {
            this.update(targetFramesPassed);
            };
    }

    setStyle(style)
    {
        this.style = style;
    }

    getNumCharsToShow() 
    {
        return Math.floor(this.elapsed * this.charsPerSec);
    }

    getCurrentTextBlockLines()
    {
        return PIXI.TextMetrics.measureText(this.textBlocks[0], this.style, this.style.wordWrap, this.canvas).lines;
    }

    getCurrentTextBlockText()
    {
        return this.textBlocks[0];
    }

    getCurrentTextBlockTotalCharsNum()
    {
        return this.textBlocks[0].length;
    }

    curTextBlockShown()
    {
        return this.getNumCharsToShow() >= this.getCurrentTextBlockTotalCharsNum() + 1
    }

    finished()
    {
        if (this.textBlocks.length == 0)
        {
            return true;
        }

        if (this.curTextBlockShown())
        {
            if (this.textBlocks.length == 1)
            {
                return true;
            }
        }

        return false;
    }

    update(targetFramesPassed)
    {
        let charsToShowBefore = this.getNumCharsToShow();

        let targetTimePerFrame = 1 / (PIXI.settings.TARGET_FPMS * 1000);
        this.elapsed += targetTimePerFrame * targetFramesPassed;

        if (charsToShowBefore != this.getNumCharsToShow())
        {
            this.dirty = true;
            if (this.curTextBlockShown())
            {
                this.tryFinish();
            }
        }
    }

    tryFinish()
    {
        if (this.finishCallback)
        {
            this.stop();

            const callback = this.finishCallback;

            if (this.finished())
            {
                this.finishCallback = null;
            }

            callback(this);
        }
    }

    stop()
    {
        PIXI.ticker.shared.remove(this.tickerFunction);
    }

    startCurrentTextBlock()
    {
        this.stop();
        
        this.elapsed = 0;

        PIXI.ticker.shared.add(this.tickerFunction);
    }

    start(callback)
    {
        this.finishCallback = callback;

        this.prepareText();
        this.startCurrentTextBlock();
    }

    startWithText(text, callback)
    {
        this.text = text;
        this.start(callback);
    }

    finishCurrentBlock()
    {
        if (!this.curTextBlockShown())
        {
            this.elapsed = 1 / this.charsPerSec * (this.getCurrentTextBlockTotalCharsNum() + 2);
            this.update(0);
        }
    }

    nextBlock()
    {
        if (this.textBlocks.length <= 1)
        {
            return;
        }

        this.textBlocks.shift();
        this.startCurrentTextBlock();
    }

    getJoinedLines(lines, numLinesToJoin)
    {
        if (!numLinesToJoin)
        {
            numLinesToJoin = lines.length;
        }
        return lines.slice(0, numLinesToJoin).reduce((acc, cur) => acc + " " + cur);
    }

    prepareText()
    {
        this.textBlocks = [];
        const measuredText = PIXI.TextMetrics.measureText(this.text, this.style, this.style.wordWrap, this.canvas);

        while (measuredText.lines.length != 0)
        {
            let num = 1;
            for (num; num <= measuredText.lines.length; num++)
            {
                const textToTest = this.getJoinedLines(measuredText.lines, num + 1);
                const measuredTestText = PIXI.TextMetrics.measureText(textToTest, this.style, this.style.wordWrap, this.canvas);
                if (measuredTestText.height > this.rect.height)
                {
                    break;
                }
            }
            this.textBlocks.push(this.getJoinedLines(measuredText.lines, num));
            measuredText.lines.splice(0, num)
        }
    }

    cutLinesForDisplay(lines)
    {
        let resultLines = [];
        let written = 0;
        for (let line of lines)
        {
            if (written + line.length < this.getNumCharsToShow())
            {
                written += line.length;
                resultLines.push(line);
            }
            else
            {
                resultLines.push(line.substring(0, this.getNumCharsToShow() - written));
                break;
            }
         }
        return resultLines;
    }

    //99% copy paste from PIXI.Rect
    updateText(respectDirty)
    {
        const style = this._style;

        // check if style has changed..
        if (this.localStyleID !== style.styleID)
        {
            this.dirty = true;
            this.localStyleID = style.styleID;
        }

        if (!this.dirty && respectDirty)
        {
            return;
        }

        this._font = this._style.toFontString();

        const context = this.context;
        const measured = PIXI.TextMetrics.measureText(this.getCurrentTextBlockText(), this._style, this._style.wordWrap, this.canvas);
        const width = measured.width;
        const height = measured.height;
        const lines = this.cutLinesForDisplay(measured.lines);
        const lineHeight = measured.lineHeight;
        const lineWidths = measured.lineWidths;
        const maxLineWidth = measured.maxLineWidth;
        const fontProperties = measured.fontProperties;

        this.canvas.width = Math.ceil((Math.max(1, width) + (style.padding * 2)) * this.resolution);
        this.canvas.height = Math.ceil((Math.max(1, height) + (style.padding * 2)) * this.resolution);

        context.scale(this.resolution, this.resolution);

        context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        context.font = this._font;
        context.strokeStyle = style.stroke;
        context.lineWidth = style.strokeThickness;
        context.textBaseline = style.textBaseline;
        context.lineJoin = style.lineJoin;
        context.miterLimit = style.miterLimit;

        let linePositionX;
        let linePositionY;

        if (style.dropShadow)
        {
            context.fillStyle = style.dropShadowColor;
            context.globalAlpha = style.dropShadowAlpha;
            context.shadowBlur = style.dropShadowBlur;

            if (style.dropShadowBlur > 0)
            {
                context.shadowColor = style.dropShadowColor;
            }

            const xShadowOffset = Math.cos(style.dropShadowAngle) * style.dropShadowDistance;
            const yShadowOffset = Math.sin(style.dropShadowAngle) * style.dropShadowDistance;

            for (let i = 0; i < lines.length; i++)
            {
                linePositionX = style.strokeThickness / 2;
                linePositionY = ((style.strokeThickness / 2) + (i * lineHeight)) + fontProperties.ascent;

                if (style.align === 'right')
                {
                    linePositionX += maxLineWidth - lineWidths[i];
                }
                else if (style.align === 'center')
                {
                    linePositionX += (maxLineWidth - lineWidths[i]) / 2;
                }

                if (style.fill)
                {
                    this.drawLetterSpacing(
                        lines[i],
                        linePositionX + xShadowOffset + style.padding, linePositionY + yShadowOffset + style.padding
                    );

                    if (style.stroke && style.strokeThickness)
                    {
                        context.strokeStyle = style.dropShadowColor;
                        this.drawLetterSpacing(
                            lines[i],
                            linePositionX + xShadowOffset + style.padding, linePositionY + yShadowOffset + style.padding,
                            true
                        );
                        context.strokeStyle = style.stroke;
                    }
                }
            }
        }

        // reset the shadow blur and alpha that was set by the drop shadow, for the regular text
        context.shadowBlur = 0;
        context.globalAlpha = 1;

        // set canvas text styles
        context.fillStyle = this._generateFillStyle(style, lines);

        // draw lines line by line
        for (let i = 0; i < lines.length; i++)
        {
            linePositionX = style.strokeThickness / 2;
            linePositionY = ((style.strokeThickness / 2) + (i * lineHeight)) + fontProperties.ascent;

            if (style.align === 'right')
            {
                linePositionX += maxLineWidth - lineWidths[i];
            }
            else if (style.align === 'center')
            {
                linePositionX += (maxLineWidth - lineWidths[i]) / 2;
            }

            if (style.stroke && style.strokeThickness)
            {
                this.drawLetterSpacing(
                    lines[i],
                    linePositionX + style.padding,
                    linePositionY + style.padding,
                    true
                );
            }

            if (style.fill)
            {
                this.drawLetterSpacing(
                    lines[i],
                    linePositionX + style.padding,
                    linePositionY + style.padding
                );
            }
        }

        this.updateTexture();
    }
}
