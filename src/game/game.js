import TypingText from "./typing-text.js";
import ClickIndicator from "./click-indicator.js";
import * as keyboard from './keyboard.js';

function createApp(conf)
{
    const app = new PIXI.Application({
        resolution: 0.5,
        width: conf.width,
        height: conf.height
    });
    document.body.appendChild(app.view);
    return app;    
}

function createTextBg(conf)
{
    const textBg = PIXI.Sprite.fromImage(conf.image);
    textBg.x = conf.rect.x;
    textBg.y = conf.rect.y;
    textBg.width = conf.rect.width;
    textBg.height = conf.rect.height;
    return textBg;
}

function createTextClickDetector(hitArea)
{
    const textClickDetector = new PIXI.Container();
    textClickDetector.interactive = true;
    textClickDetector.hitArea = hitArea;
    return textClickDetector;
}

function createTextPresenter(conf)
{
    const typingText = new TypingText(
        "", 
        conf.style,
        conf.rect,
        conf.charsPerSec
        );
    return typingText;
}

function createNameDisplayer(conf)
{
    let nameDisplayer = new PIXI.Text("", conf.style);
    nameDisplayer.position = conf.position;
    return nameDisplayer;
}

export default class Game
{
    constructor(conf)
    {
        this.conf = conf;
        this.app = createApp(conf.app);

        this.scene = this.app.stage.addChild(new PIXI.Container());
        this.interface = this.app.stage.addChild(new PIXI.Container());

        this.textBg = createTextBg(conf.textBg);
        this.clickIndicator = new ClickIndicator(this.app.ticker, conf.clickIndicator);
        this.textPresenter = createTextPresenter(conf.textPresenter);
        this.textClickDetector = createTextClickDetector(this.app.screen);
        this.nameDisplayer = createNameDisplayer(conf.nameDisplayer);

        this.interface.addChild(
            this.textClickDetector, 
            this.textBg,
            this.nameDisplayer,
            this.textPresenter,
            this.clickIndicator
            );

        if (this.conf.developer)
        {
            let ctrl = keyboard.ctrl();
            let intervalId = null;
            ctrl.press = () => {
                let skipper = () => {
                    if (ctrl.isDown)
                    {
                        this.textClickDetector.emit('pointerdown');
                    }
                };
                intervalId = setInterval(skipper, 25);
            }
            ctrl.release = () => {
                clearInterval(intervalId);
            }
        }
    }   
    
    typingText(text, textStyle, onUserFinishedWithIt)
    {
        this.clickIndicator.hide();
        this.textClickDetector.removeAllListeners('pointerdown');

        const textShowFinishCallback = () => {
            this.clickIndicator.show();
        };

        if (textStyle === undefined)
        {
            textStyle = this.conf.textPresenter.style;
        }
        this.textPresenter.setStyle(textStyle);
        this.textPresenter.startWithText(text, textShowFinishCallback);

        let clickFunc = () => {
            if (this.textPresenter.finished())
            {
                this.clickIndicator.hide();
                this.textClickDetector.removeAllListeners('pointerdown');
                if (onUserFinishedWithIt)
                    onUserFinishedWithIt();
            }
            else 
            {
                if (this.textPresenter.curTextBlockShown())
                {
                    this.clickIndicator.hide();
                    this.textPresenter.nextBlock();
                }
                else
                {
                    this.textPresenter.finishCurrentBlock();
                }
            }
        };

        this.textClickDetector.on('pointerdown', clickFunc);
    } 

    menu(text, items, callback)
    {
        this.typingText(text);

        let clicked = -1;

        let center = Object.assign({}, this.conf.menu.center);
        center.y -= items.length * this.conf.menu.elementHeight / 2;

        let elements = [];
        for (let i = 0; i < items.length; i++)
        {
            const button = PIXI.Sprite.fromImage(this.conf.menu.buttons.image);
            button.anchor.x = 0.5;
            button.x = center.x;
            button.y = center.y + i * this.conf.menu.elementHeight;
            button.width = this.conf.menu.buttons.rect.width;
            button.height = this.conf.menu.buttons.rect.height;

            button.interactive = true;
            button.on('pointerdown', () => {
                this.interface.removeChild(...elements);
                callback(i);
                });

            const text = new PIXI.Text(items[i], this.conf.menu.buttons.textStyle);
            text.anchor.x = 0.5;
            text.x = center.x;
            text.y = center.y + i * this.conf.menu.elementHeight;

            elements.push(button, text);
        }
        this.interface.addChild(...elements);
    }

    get stage()
    {
        return this.scene;
    }

    setName(name, style)
    {
        this.nameDisplayer.text = name;
        if (style !== undefined)
        {
            this.nameDisplayer.style = style;
        }
    }
}

