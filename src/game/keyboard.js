export function ctrl()
{
    let ctrl = {};
    ctrl.isDown = false;
    ctrl.isUp = true;
    ctrl.press = undefined;
    ctrl.release = undefined;
    //The `downHandler`
    ctrl.downHandler = event => {
        if (event.ctrlKey) {
            if (ctrl.isUp && ctrl.press) ctrl.press();
            ctrl.isDown = true;
            ctrl.isUp = false;
        }
        // event.preventDefault();
    };

    //The `upHandler`
    ctrl.upHandler = event => {
        if (!event.ctrlKey) {
            if (ctrl.isDown && ctrl.release) ctrl.release();
            ctrl.isDown = false;
            ctrl.isUp = true;
        }
        // event.preventDefault();
    };

    //Attach event listeners
    window.addEventListener(
        "keydown", ctrl.downHandler.bind(ctrl), false
    );
    window.addEventListener(
        "keyup", ctrl.upHandler.bind(ctrl), false
    );
    return ctrl;
}
