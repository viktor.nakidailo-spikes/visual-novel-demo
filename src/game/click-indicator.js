export default class ClickIndicator extends PIXI.Container
{
    constructor(ticker, conf)
    {
        super();

        this.ticker = ticker

        this.clickIndicator = PIXI.Sprite.fromImage(conf.image);
        this.clickIndicator.x = conf.position.x;
        this.clickIndicator.y = conf.position.y;
        this.clickIndicator.visible = false;

        this.alphaAnimDirection = 1;

        this.updateFunc = (tfp) => {
            if (this.alphaAnimDirection > 0)
            {
                if (this.clickIndicator.alpha >= 1)
                {
                    this.reverseAlphaAnimDirection();
                }
            }
            else
            {
                if (this.clickIndicator.alpha <= 0)
                {
                    this.reverseAlphaAnimDirection();
                }
            }

            this.clickIndicator.alpha += 0.1 * tfp * this.alphaAnimDirection;
        }

        this.addChild(this.clickIndicator);
    }

    reverseAlphaAnimDirection()
    {
        this.alphaAnimDirection = -this.alphaAnimDirection;
    }

    show()
    {
        this.clickIndicator.visible = true;
        this.clickIndicator.alpha = 0;
        this.alphaAnimDirection = 1;

        this.ticker.add(this.updateFunc);
    }

    hide()
    {
        this.clickIndicator.visible = false;
        this.ticker.remove(this.updateFunc);
    }
}
